package mazegame.listener;

import mazegame.movement.MovementDirection;

public interface InputListener {
    void onMove(MovementDirection direction);
}
