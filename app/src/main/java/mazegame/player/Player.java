package mazegame.player;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import androidx.core.content.ContextCompat;
import mazegame.dot.Dot;
import mazegame.R;

public class Player extends Dot {

    public Player(Context context, Point start, int size) {
        super(size, start, getPaint(context));
    }

    private static Paint getPaint(Context context) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(ContextCompat.getColor(context, R.color.gm_player));
        return paint;
    }
}
