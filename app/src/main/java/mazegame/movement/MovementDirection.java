package mazegame.movement;

public enum MovementDirection {
    LEFT, UP, RIGHT, DOWN
}
